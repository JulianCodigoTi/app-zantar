import {
  Component
} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public dataUsuario: any;

  constructor() {
    const data = JSON.parse(localStorage.getItem("clienteLogin"));
    this.dataUsuario = data.clienteLogin;
    console.log(this.dataUsuario);
  }

}