import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { AuthserviceService } from '../authservice/authservice.service';

@Component({
  selector: 'app-tipocuenta',
  templateUrl: './tipocuenta.page.html',
  styleUrls: ['./tipocuenta.page.scss'],
})
export class TipocuentaPage implements OnInit {
  public  tipocuenta = {
    "descripcion": ""
  }
  public editar = false;
  public tipoactualizacion: any;
  public resposeData:any;
  public resposeDataON: any;
  public filter:any;
  public dataTipoUsuario: any;

  constructor(public authService: AuthserviceService, private snackBar: MatSnackBar) { }

  ngOnInit(){
    this.resposeDataON = [];
    this.authService.getData("consultatipocuenta").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultatipocuenta){
       this.resposeDataON = this.resposeData.consultatipocuenta;
       this.filter = this.resposeData.consultatipocuenta;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  agregar(){
    this.editar = true;
    this.tipocuenta = { "descripcion": ""};
    this.tipoactualizacion = "inserta";
  }

  editarR(data){
    this.editar = true;
    this.tipoactualizacion = "edita";
    this.tipocuenta = data;
  }

  guardar(){
    if(this.tipoactualizacion == "inserta"){
    this.authService.postData(this.tipocuenta, "registrotipocuenta").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Guardado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
   if(this.tipoactualizacion == "edita"){
    this.authService.postData(this.tipocuenta, "updatetipocuenta").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Registro editado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
  }

  eliminar(data){
    this.authService.postData(data, "deletetipocuenta").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
        this.snackBar.open("Exito","Eliminacion exitosa.",{
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      }else{
       this.snackBar.open("Error","Registro no eliminado.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }else{
     this.resposeDataON = this.resposeDataON.filter(item => {
      if (item.descripcion && searchTerm) {
        if (item.descripcion.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
     });
    }
  }
}
