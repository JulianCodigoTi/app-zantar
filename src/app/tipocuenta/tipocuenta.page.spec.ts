import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipocuentaPage } from './tipocuenta.page';

describe('TipocuentaPage', () => {
  let component: TipocuentaPage;
  let fixture: ComponentFixture<TipocuentaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipocuentaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipocuentaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
