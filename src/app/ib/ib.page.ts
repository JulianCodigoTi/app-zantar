import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../authservice/authservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-ib',
  templateUrl: './ib.page.html',
  styleUrls: ['./ib.page.scss'],
})
export class IbPage implements OnInit {
  public  caib = {
    "prefijo": "",
    "empresa": ""
  }
  public editar = false;
  public tipoactualizacion: any;
  public resposeData:any;
  public resposeDataON: any;
  public filter:any;

  constructor(public authService: AuthserviceService, private snackBar: MatSnackBar) { }

  ngOnInit(){
    this.resposeDataON = [];
    this.authService.getData("consultacaib").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacaib){
       this.resposeDataON = this.resposeData.consultacaib;
       this.filter = this.resposeData.consultacaib;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  agregar(){
    this.editar = true;
    this.caib = { "prefijo": "", "empresa": "" };
    this.tipoactualizacion = "inserta";
  }

  editarR(data){
    this.editar = true;
    this.tipoactualizacion = "edita";
    this.caib = data;
  }

  guardar(){
    if(this.tipoactualizacion == "inserta"){
    this.authService.postData(this.caib, "registrocaib").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Guardado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
   if(this.tipoactualizacion == "edita"){
    this.authService.postData(this.caib, "updatecaib").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Registro editado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
  }

  eliminar(data){
    this.authService.postData(data, "deletecaib").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
        this.snackBar.open("Exito","Eliminacion exitosa.",{
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      }else{
       this.snackBar.open("Error","Registro no eliminado.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }else{
     this.resposeDataON = this.resposeDataON.filter(item => {
       console.log("filter");
       console.log(item);
      if (item.empresa && searchTerm || item.prefijo && searchTerm) {
        if ((item.empresa.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) ||
            (item.prefijo.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)) {
          return true;
        }
        return false;
      }
     });
    }
  }
}
