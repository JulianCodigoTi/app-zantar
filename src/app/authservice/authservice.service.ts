import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//let apiUrl = 'http://localhost/PHP-Slim-BKTrading/api/index.php/';
let apiUrl = 'http://192.168.0.11/PHP-Slim-BKTrading/api/index.php/';

@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {

  constructor(public http:HttpClient) { }

  postData(credentials, type) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      this.http.post(apiUrl + type, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getData(type) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      this.http.get(apiUrl + type,  {headers: headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}
