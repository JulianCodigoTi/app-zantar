import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'home',  loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
  { path: 'list',  loadChildren: () => import('./list/list.module').then(m => m.ListPageModule) },
  { path: 'tipocuenta', loadChildren: './tipocuenta/tipocuenta.module#TipocuentaPageModule' },
  { path: 'tipousuario', loadChildren: './tipousuario/tipousuario.module#TipousuarioPageModule' },
  { path: 'tipomovimiento', loadChildren: './tipomovimiento/tipomovimiento.module#TipomovimientoPageModule' },
  { path: 'subtipomovimiento', loadChildren: './subtipomovimiento/subtipomovimiento.module#SubtipomovimientoPageModule' },
  { path: 'ib', loadChildren: './ib/ib.module#IbPageModule' },
  { path: 'intrumentos', loadChildren: './intrumentos/intrumentos.module#IntrumentosPageModule' },
  { path: 'causuario', loadChildren: './causuario/causuario.module#CausuarioPageModule' },
  { path: 'cacliente', loadChildren: './cacliente/cacliente.module#CaclientePageModule' },
  { path: 'camovimiento', loadChildren: './camovimiento/camovimiento.module#CamovimientoPageModule' },
  { path: 'trades', loadChildren: './trades/trades.module#TradesPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
