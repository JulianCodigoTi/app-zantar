import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../authservice/authservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-trades',
  templateUrl: './trades.page.html',
  styleUrls: ['./trades.page.scss'],
})
export class TradesPage implements OnInit {
  public  movimiento = {
    "idcliente":"",
    "idtipomovimiento":"",
    "idsubtipomovimiento":"",
    "monto":"",
    "nombrebeneficiario":"",
    "cuenta":"",
    "banco":"",
    "numeroagenciasucursal":"",
    "ciudad":"",
    "pais":"",
    "abaiba":"",
    "swiftchip":"",
    "instruccionesespeciales":"",
    "bancoemisor":"",
    "paisbancoemisor":"",
    "localidadbancoemisor":"",
    "fecmaxtransferencia":""  
  };
  public editar = false;
  public data   = false;
  public opc = false;
  public tipoactualizacion: any;
  public resposeData:any;
  public resposeDataON: any;
  public resposeDataCliente: any;
  public resposeDataTipoMovimiento: any;
  public resposeDataSubTipo: any;
  public filter:any;

  aux :any;

  constructor(public authService: AuthserviceService, private snackBar: MatSnackBar) { }

  ngOnInit(){
    this.resposeDataON = [];
    this.authService.getData("consultacacliente").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacacliente){
       this.resposeDataCliente = this.resposeData.consultacacliente;
       console.log(this.resposeDataCliente); 
      }
    });

    this.authService.getData("consultacamovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacamovimiento){
       this.resposeDataON = this.resposeData.consultacamovimiento;
       console.log(this.resposeDataON);
       this.filter = this.resposeData.consultacamovimiento;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });

    this.authService.getData("consultatipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultatipomovimiento){
       this.resposeDataTipoMovimiento = this.resposeData.consultatipomovimiento;
      }
    });

  }

  agregar(){
    this.editar = true;
    this.opc = false;
    this.movimiento = { "idcliente":"",
    "idtipomovimiento":"",
    "idsubtipomovimiento":"",
    "monto":"",
    "nombrebeneficiario":"",
    "cuenta":"",
    "banco":"",
    "numeroagenciasucursal":"",
    "ciudad":"",
    "pais":"",
    "abaiba":"",
    "swiftchip":"",
    "instruccionesespeciales":"",
    "bancoemisor":"",
    "paisbancoemisor":"",
    "localidadbancoemisor":"",
    "fecmaxtransferencia":""  
   }; 
    this.tipoactualizacion = "inserta";
  }

  editarR(data){
    this.editar = true;
    this.opc = false;
    this.tipoactualizacion = "edita";
    this.movimiento = data;
  }

  guardar(){
    console.log(this.movimiento);
    if(this.tipoactualizacion == "inserta"){
    this.authService.postData(this.movimiento, "registrocamovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Guardado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
   if(this.tipoactualizacion == "edita"){
    this.authService.postData(this.movimiento, "updatecamovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Registro editado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
  }

  eliminar(data){
    this.authService.postData(data, "deletecamovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
        this.snackBar.open("Exito","Eliminacion exitosa.",{
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      }else{
       this.snackBar.open("Error","Registro no eliminado.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }else{
     this.resposeDataON = this.resposeDataON.filter(item => {
      if (item.nombre && searchTerm || item.destipousuario && searchTerm) {
        if ((item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) ||
            (item.destipousuario.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)) {
          return true;
        }
        return false;
      }
     });
    }
  }

  info(data){
    console.log(data);
    this.data = true;
    this.movimiento = data;
  }

  tipo(mov){
    console.log(mov);
    this.resposeDataSubTipo = null;
    console.log(this.resposeDataSubTipo);
    var envio = {
      'idtipomovimiento' : mov
    }
    this.authService.postData(envio,"consultasubtipomovimientofilter").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultasubtipomovimientofilter){
       this.resposeDataSubTipo = this.resposeData.consultasubtipomovimientofilter;
      }
    });
  }
}