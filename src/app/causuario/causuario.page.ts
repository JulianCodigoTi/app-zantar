import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../authservice/authservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-causuario',
  templateUrl: './causuario.page.html',
  styleUrls: ['./causuario.page.scss'],
})
export class CausuarioPage implements OnInit {
  public  causuario = {
    "idtipousuario": "",
    "nombre": "",
    "ap": "",
    "am": "",
    "email":"",
    "user": "",
    "password": "",
    "telefono": "",
    "idmanager": "",
    "idcaib": ""
  }
  public editar = false;
  public opc = false;
  public tipoactualizacion: any;
  public resposeData:any;
  public resposeDataON: any;
  public resposeDataTipoUsuario: any;
  public resposeDataManager: any;
  public resposeDataIB: any;
  public filter:any;

  constructor(public authService: AuthserviceService, private snackBar: MatSnackBar) { }

  ngOnInit(){
    this.resposeDataON = [];
    this.authService.getData("consultatipousuario").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultatipousuario){
       this.resposeDataTipoUsuario = this.resposeData.consultatipousuario;
       console.log("Tipo usuario");
       console.log(this.resposeDataTipoUsuario); 
      }
    });

    this.authService.getData("consultacausuario").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacausuario){
       this.resposeDataON = this.resposeData.consultacausuario;
       console.log(this.resposeDataON);
       this.filter = this.resposeData.consultacausuario;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });

    this.authService.getData("consultacausuariomanager").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacausuariomanager){
       this.resposeDataManager = this.resposeData.consultacausuariomanager;
      }
    });

    this.authService.getData("consultacaib").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacaib){
       this.resposeDataIB = this.resposeData.consultacaib;
      }
    });

  }

  agregar(){
    this.editar = true;
    this.opc = false;
    this.causuario = {
      "idtipousuario": "",
      "nombre": "",
      "ap": "",
      "am": "",
      "email":"",
      "user": "",
      "password": "",
      "telefono": "",
      "idmanager": "",
      "idcaib": ""
    };  
    this.tipoactualizacion = "inserta";
  }

  editarR(data){
    this.editar = true;
    this.opc = false;
    this.tipoactualizacion = "edita";
    this.causuario = data;
  }

  guardar(){
    if(this.tipoactualizacion == "inserta"){
    this.authService.postData(this.causuario, "registrocausuario").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Guardado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
   if(this.tipoactualizacion == "edita"){
    this.authService.postData(this.causuario, "updatecausuario").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Registro editado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
  }

  eliminar(data){
    this.authService.postData(data, "deletecausuario").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
        this.snackBar.open("Exito","Eliminacion exitosa.",{
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      }else{
       this.snackBar.open("Error","Registro no eliminado.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }else{
     this.resposeDataON = this.resposeDataON.filter(item => {
      if (item.nombre && searchTerm || item.destipousuario && searchTerm) {
        if ((item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) ||
            (item.destipousuario.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)) {
          return true;
        }
        return false;
      }
     });
    }
  }

  info(data){
    console.log(data);
    this.editar = true;
    this.opc = true;
    this.causuario = data;

  }
}