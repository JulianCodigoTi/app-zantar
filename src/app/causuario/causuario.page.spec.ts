import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CausuarioPage } from './causuario.page';

describe('CausuarioPage', () => {
  let component: CausuarioPage;
  let fixture: ComponentFixture<CausuarioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CausuarioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CausuarioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
