import {
  Component
} from '@angular/core';
import {
  MatSnackBar
} from '@angular/material';
import {
  Router
} from '@angular/router';
import {
  AuthserviceService
} from '../authservice/authservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  dataUsuario = {
    "user": "",
    "password": ""
  };

  public resposeData: any;

  constructor(private snackBar: MatSnackBar, public authService: AuthserviceService, public router: Router) {}

  login() {
    console.log(this.dataUsuario);
    if (this.dataUsuario.user == '') {
      this.snackBar.open("Error", "Usuario requerido.", {
        duration: 3000,
        verticalPosition: "bottom"
      });
    }

    if (this.dataUsuario.password == '') {
      this.snackBar.open("Error", "Password requerido.", {
        duration: 3000,
        verticalPosition: "bottom"
      });
    }

    if (this.dataUsuario.user == '' && this.dataUsuario.password == '') {
      this.snackBar.open("Error", "Usuario y Password requeridos.", {
        duration: 3000,
        verticalPosition: "bottom"
      });
    }

    if (this.dataUsuario.user && this.dataUsuario.password) {
      this.authService.postData(this.dataUsuario, "login").then((result) => {
        this.resposeData = result;
        if (this.resposeData.clienteLogin) {
          localStorage.setItem('clienteLogin', JSON.stringify(this.resposeData))
          this.router.navigate(["home"]);
        } else {
          this.snackBar.open("Error", "Usuario y/o password erroneos.", {
            duration: 3000,
            verticalPosition: "bottom"
          });
        }
      }, (err) => {
        this.snackBar.open("Error", "Conexion al servidor.", {
          duration: 3000,
          verticalPosition: "bottom"
        });
      });
    }
  }
}