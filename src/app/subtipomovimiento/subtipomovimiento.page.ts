import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../authservice/authservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-subtipomovimiento',
  templateUrl: './subtipomovimiento.page.html',
  styleUrls: ['./subtipomovimiento.page.scss'],
})
export class SubtipomovimientoPage implements OnInit {
  public resposeData: any;
  public resposeDataON: any;
  public resposeDataTM:any;
  public resposeDataT:any;
  public filter:any;
  public dataTipomovimiento: any;
  public  subtipomovimiento = { }
  editar = false;
  public tipoactualizacion: any;

  constructor(public authService: AuthserviceService, private snackBar: MatSnackBar) { 
    
  }

  ngOnInit(){
    this.resposeDataON=[];
    this.authService.getData("consultasubtipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultasubtipomovimiento){
       this.resposeDataON = this.resposeData.consultasubtipomovimiento;
       console.log(this.resposeDataON);
       this.filter = this.resposeData.consultasubtipomovimiento;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });

    this.authService.getData("consultatipomovimiento").then((result) =>{
      this.resposeDataT = result;
      if(this.resposeDataT.consultatipomovimiento){
       this.resposeDataTM = this.resposeDataT.consultatipomovimiento;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  agregar(){
    this.editar = true;
    this.subtipomovimiento = {};
    this.tipoactualizacion = "inserta";
  }

  editarR(data){
    this.editar = true;
    this.tipoactualizacion = "edita";
    this.subtipomovimiento = data;
  }

  guardar(){
    console.log(this.subtipomovimiento);
    if(this.tipoactualizacion == "inserta"){
    this.authService.postData(this.subtipomovimiento, "registrosubtipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Guardado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
   if(this.tipoactualizacion == "edita"){
     console.log("edita");
     console.log(this.subtipomovimiento); 
    this.authService.postData(this.subtipomovimiento, "updatesubtipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Registro editado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
  }

  eliminar(data){
    this.authService.postData(data, "deletesubtipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
        this.snackBar.open("Exito","Eliminacion exitosa.",{
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      }else{
       this.snackBar.open("Error","Registro no eliminado.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }else{
     this.resposeDataON = this.resposeDataON.filter(item => {
       console.log("filter");
       console.log(item);
      if (item.destipomovimiento && searchTerm || item.descripcion && searchTerm) {
        if ((item.destipomovimiento.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) ||
            (item.descripcion.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)) {
          return true;
        }
        return false;
      }
     });
    }
  }
}
