import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SubtipomovimientoPage } from './subtipomovimiento.page';

const routes: Routes = [
  {
    path: '',
    component: SubtipomovimientoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SubtipomovimientoPage]
})
export class SubtipomovimientoPageModule {}
