import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubtipomovimientoPage } from './subtipomovimiento.page';

describe('SubtipomovimientoPage', () => {
  let component: SubtipomovimientoPage;
  let fixture: ComponentFixture<SubtipomovimientoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubtipomovimientoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtipomovimientoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
