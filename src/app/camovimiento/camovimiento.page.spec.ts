import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamovimientoPage } from './camovimiento.page';

describe('CamovimientoPage', () => {
  let component: CamovimientoPage;
  let fixture: ComponentFixture<CamovimientoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamovimientoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamovimientoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
