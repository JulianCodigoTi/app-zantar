import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../authservice/authservice.service';
import { MatSnackBar } from '@angular/material';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Platform } from '@ionic/angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

declare var cordova:any;

@Component({
  selector: 'app-camovimiento',
  templateUrl: './camovimiento.page.html',
  styleUrls: ['./camovimiento.page.scss'],
})
export class CamovimientoPage implements OnInit {
  public movimiento = {
    "idcliente": "",
    "idtipomovimiento": "",
    "idsubtipomovimiento": "",
    "monto": "",
    "nombrebeneficiario": "",
    "cuenta": "",
    "banco": "",
    "numeroagenciasucursal": "",
    "ciudad": "",
    "pais": "",
    "abaiba": "",
    "swiftchip": "",
    "instruccionesespeciales": "",
    "bancoemisor": "",
    "paisbancoemisor": "",
    "localidadbancoemisor": "",
    "fecmaxtransferencia": "",
    "bancointermediario": false,
    "nombrebeneficiariobi": "",
    "cuentabi": "",
    "bancobi": "",
    "numeroagenciasucursalbi": "",
    "ciudadbi": "",
    "paisbi": "",
    "abaibabi": "",
    "swiftchipbi": "",
    "instruccionesespecialesbi": "",
     account:0,
     creditline:0,
     operativemargin: 0
  };
  public editar = false;
  public data = false;
  public opc = false;
  public tipoactualizacion: any;
  public resposeData: any;
  public resposeDataON: any;
  public resposeDataCliente: any;
  public resposeDataTipoMovimiento: any;
  public resposeDataSubTipo: any;
  public filter: any;
  
  aux: any;

  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};

  constructor(public authService: AuthserviceService,
    private snackBar: MatSnackBar,
    private file: File,
    private fileOpener: FileOpener,
    public platform: Platform,
    private inAppBrowser: InAppBrowser) {}

  ngOnInit() {
    this.resposeDataON = [];
    this.authService.getData("consultacacliente").then((result) => {
      this.resposeData = result;
      if (this.resposeData.consultacacliente) {
        this.resposeDataCliente = this.resposeData.consultacacliente;
        console.log(this.resposeDataCliente);
      }
    });

    this.authService.getData("consultacamovimiento").then((result) => {
      this.resposeData = result;
      if (this.resposeData.consultacamovimiento) {
        this.resposeDataON = this.resposeData.consultacamovimiento;
        console.log(this.resposeDataON);
        this.filter = this.resposeData.consultacamovimiento;
      } else {
        this.snackBar.open("Error", "Infomacion no disponible.", {
          duration: 3000,
          verticalPosition: "bottom"
        });
      }
    });

    this.authService.getData("consultatipomovimiento").then((result) => {
      this.resposeData = result;
      if (this.resposeData.consultatipomovimiento) {
        this.resposeDataTipoMovimiento = this.resposeData.consultatipomovimiento;
      }
    });

  }

  agregar() {
    this.editar = true;
    this.opc = false;
    this.movimiento = {
      "idcliente": "",
      "idtipomovimiento": "",
      "idsubtipomovimiento": "",
      "monto": "",
      "nombrebeneficiario": "",
      "cuenta": "",
      "banco": "",
      "numeroagenciasucursal": "",
      "ciudad": "",
      "pais": "",
      "abaiba": "",
      "swiftchip": "",
      "instruccionesespeciales": "",
      "bancoemisor": "",
      "paisbancoemisor": "",
      "localidadbancoemisor": "",
      "fecmaxtransferencia": "",
      "bancointermediario": false,
      "nombrebeneficiariobi": "",
      "cuentabi": "",
      "bancobi": "",
      "numeroagenciasucursalbi": "",
      "ciudadbi": "",
      "paisbi": "",
      "abaibabi": "",
      "swiftchipbi": "",
      "instruccionesespecialesbi": "",
       account:0,
       creditline:0,
       operativemargin: 0
    };
    this.tipoactualizacion = "inserta";
  }

  editarR(data) {
    console.log(data);
    this.editar = true;
    this.opc = false;
    this.tipoactualizacion = "edita";
    this.movimiento = data;
    if (data.idmovimientorepadriacion != null && data.idmovimientorepadriacion != '') {
      this.movimiento.bancointermediario = true;
    }

  }

  guardar() {
    if (this.tipoactualizacion == "inserta") {
      this.authService.postData(this.movimiento, "registrocamovimiento").then((result) => {
        this.resposeData = result;
        if (this.resposeData.exito) {
          this.snackBar.open("Exito", "Guardado exitosamente.", {
            duration: 3000,
            verticalPosition: "bottom"
          });
          this.editar = false;
          this.ngOnInit();
        } else {
          this.snackBar.open("Error", "No se guardo el registro.", {
            duration: 3000,
            verticalPosition: "bottom"
          });
          this.editar = false;
        }
      });
    }
    if (this.tipoactualizacion == "edita") {
      console.log(this.movimiento);
      this.authService.postData(this.movimiento, "updatecamovimiento").then((result) => {
        this.resposeData = result;
        if (this.resposeData.exito) {
          this.snackBar.open("Exito", "Registro editado exitosamente.", {
            duration: 3000,
            verticalPosition: "bottom"
          });
          this.editar = false;
          this.ngOnInit();
        } else {
          this.snackBar.open("Error", "No se guardo el registro.", {
            duration: 3000,
            verticalPosition: "bottom"
          });
          this.editar = false;
        }
      });
    }
  }

  eliminar(data) {
    this.authService.postData(data, "deletecamovimiento").then((result) => {
      this.resposeData = result;
      if (this.resposeData.exito) {
        this.snackBar.open("Exito", "Eliminacion exitosa.", {
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      } else {
        this.snackBar.open("Error", "Registro no eliminado.", {
          duration: 3000,
          verticalPosition: "bottom"
        });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    } else {
      this.resposeDataON = this.resposeDataON.filter(item => {
        if (item.numerocuenta && searchTerm || item.descripcionmovimiento && searchTerm) {
          if ((item.numerocuenta.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) ||
            (item.descripcionmovimiento.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)) {
            return true;
          }
          return false;
        }
      });
    }
  }

  info(data) {
    console.log(data);
    this.data = true;
    this.movimiento = data;
    if (data.idmovimientorepadriacion != null && data.idmovimientorepadriacion != '') {
      this.movimiento.bancointermediario = true;
    }

    var totalcredito = 0;
    var totaldebito  = 0;
    
    for(var i = 0; i < this.resposeDataON.length; i++){
       if(this.resposeDataON[i].idcliente == this.movimiento.idcliente){
         if(this.resposeDataON[i].descripcionmovimiento == 'CREDITO'){
             totalcredito = totalcredito + parseInt(this.resposeDataON[i].monto,10);       
         }
         if(this.resposeDataON[i].descripcionmovimiento == 'DEBITO'){
             totaldebito = totaldebito + parseInt(this.resposeDataON[i].monto,10);       
         }
         if(this.resposeDataON[i].descripcionmovimiento == 'DEPOSITO INICIAL'){
             totaldebito = totaldebito + parseInt(this.resposeDataON[i].monto,10);       
         }
         /*
         if(this.resposeDataON[i].descripcionmovimiento == 'WITHDRAWAL'){
          totaldebito = totaldebito - parseInt(this.resposeDataON[i].monto,10);       
         }
         */
       }
    }

    this.movimiento.account         = totaldebito;
    this.movimiento.creditline      = totalcredito;
    this.movimiento.operativemargin = totalcredito + totaldebito;


  }

  tipo(mov) {
    console.log(mov);
    this.resposeDataSubTipo = null;
    console.log(this.resposeDataSubTipo);
    var envio = {
      'idtipomovimiento': mov
    }
    this.authService.postData(envio, "consultasubtipomovimientofilter").then((result) => {
      this.resposeData = result;
      if (this.resposeData.consultasubtipomovimientofilter) {
        this.resposeDataSubTipo = this.resposeData.consultasubtipomovimientofilter;
      }
    });
  }

  valor(event) {
    console.log(event.detail.checked);
    this.movimiento.bancointermediario = event.detail.checked;
  }

  makePdf(movimiento) {
    console.log(movimiento);
    var objeto = JSON.stringify(movimiento);
    for(var i = 0; i < objeto.length; i++){
      objeto = objeto.replace('"', '' ); 
      objeto = objeto.replace(':', '='); 
      objeto = objeto.replace(',', '&'); 
      objeto = objeto.replace('{', ''); 
      objeto = objeto.replace('}', '');
    }
    let target = "_system";
    this.inAppBrowser.create("http://192.168.0.11/PHP-Slim-BKTrading/api/pdf.php?"+objeto,target,this.options);
    console.log(this.file.externalRootDirectory);
    //this.fileOpener.open(this.file.externalRootDirectory + '/Download/mpdf(1).pdf', 'application/pdf');
    //this.inAppBrowser.create("http://192.168.0.11/PHP-Slim-BKTrading/api/pdf.php?"+objeto);
  }
}