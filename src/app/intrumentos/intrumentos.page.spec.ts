import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntrumentosPage } from './intrumentos.page';

describe('IntrumentosPage', () => {
  let component: IntrumentosPage;
  let fixture: ComponentFixture<IntrumentosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntrumentosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntrumentosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
