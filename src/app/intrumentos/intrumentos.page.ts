import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../authservice/authservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-intrumentos',
  templateUrl: './intrumentos.page.html',
  styleUrls: ['./intrumentos.page.scss'],
})
export class IntrumentosPage implements OnInit {
  public  cainstrumentos = {
    "tipo": "",
    "nombre": "",
    "notas": "",
    "valordolares": "",
    "multiplicador":"",
    "riesgo": "",
    "comision": "",
    "porcentajecomision": "",
    "estatus": ""
  }
  public opcionesArr = ['Si', 'No'];
  public editar = false;
  public opc = false;
  public tipoactualizacion: any;
  public resposeData:any;
  public resposeDataON: any;
  public filter:any;

  constructor(public authService: AuthserviceService, private snackBar: MatSnackBar) { }

  ngOnInit(){
    this.resposeDataON = [];
    this.authService.getData("consultacainstrumento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacainstrumento){
       this.resposeDataON = this.resposeData.consultacainstrumento;
       this.filter = this.resposeData.consultacainstrumento;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  agregar(){
    this.editar = true;
    this.opc = false;
    this.cainstrumentos = { 
      "tipo": "",
      "nombre": "",
      "notas": "",
      "valordolares": "",
      "multiplicador":"",
      "riesgo": "",
      "comision": "",
      "porcentajecomision": "",
      "estatus": ""
    };
    this.tipoactualizacion = "inserta";
  }

  editarR(data){
    this.editar = true;
    this.opc = false;
    this.tipoactualizacion = "edita";
    this.cainstrumentos = data;
  }

  guardar(){
    if(this.tipoactualizacion == "inserta"){
    this.authService.postData(this.cainstrumentos, "registrocainstrumento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Guardado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
   if(this.tipoactualizacion == "edita"){
    this.authService.postData(this.cainstrumentos, "updatecainstrumento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Registro editado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
  }

  eliminar(data){
    this.authService.postData(data, "deletecainstrumento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
        this.snackBar.open("Exito","Eliminacion exitosa.",{
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      }else{
       this.snackBar.open("Error","Registro no eliminado.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }else{
     this.resposeDataON = this.resposeDataON.filter(item => {
       console.log("filter");
       console.log(item);
      if (item.tipo && searchTerm || item.nombre && searchTerm) {
        if ((item.tipo.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) ||
            (item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)) {
          return true;
        }
        return false;
      }
     });
    }
  }

  info(data){
    console.log(data);
    this.editar = true;
    this.opc = true;
    this.cainstrumentos = data;

  }

  valor(event){
    console.log(event.detail.checked);

    this.cainstrumentos.valordolares = event.detail.checked;
    
  }
}