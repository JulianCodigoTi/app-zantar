import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../authservice/authservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-tipomovimiento',
  templateUrl: './tipomovimiento.page.html',
  styleUrls: ['./tipomovimiento.page.scss'],
})
export class TipomovimientoPage implements OnInit {
  public resposeData: any;
  public resposeDataON: any;
  public filter:any;
  public dataTipomovimiento: any;
  public  tipomovimiento = {
            "descripcion": ""
          }
  editar = false;
  public tipoactualizacion: any;

  constructor(public authService: AuthserviceService, private snackBar: MatSnackBar) { 
    
  }

  ngOnInit(){
    this.resposeDataON = [];
    this.authService.getData("consultatipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultatipomovimiento){
       this.resposeDataON = this.resposeData.consultatipomovimiento;
       this.filter = this.resposeData.consultatipomovimiento;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  agregar(){
    this.editar = true;
    this.tipomovimiento = {"descripcion": ""};
    this.tipoactualizacion = "inserta";
  }

  editarR(data){
    this.editar = true;
    this.tipoactualizacion = "edita";
    this.tipomovimiento = data;
  }

  guardar(){
    if(this.tipoactualizacion == "inserta"){
    this.authService.postData(this.tipomovimiento, "registrotipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Guardado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
   if(this.tipoactualizacion == "edita"){
    this.authService.postData(this.tipomovimiento, "updatetipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Registro editado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
  }

  eliminar(data){
    this.authService.postData(data, "deletetipomovimiento").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
        this.snackBar.open("Exito","Eliminacion exitosa.",{
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      }else{
       this.snackBar.open("Error","Registro no eliminado.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }else{
     this.resposeDataON = this.resposeDataON.filter(item => {
      if (item.descripcion && searchTerm) {
        if (item.descripcion.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
     });
    }
  }
}
