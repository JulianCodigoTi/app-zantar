import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipomovimientoPage } from './tipomovimiento.page';

describe('TipomovimientoPage', () => {
  let component: TipomovimientoPage;
  let fixture: ComponentFixture<TipomovimientoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipomovimientoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipomovimientoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
