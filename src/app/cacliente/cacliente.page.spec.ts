import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaclientePage } from './cacliente.page';

describe('CaclientePage', () => {
  let component: CaclientePage;
  let fixture: ComponentFixture<CaclientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaclientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaclientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
