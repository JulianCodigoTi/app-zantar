import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../authservice/authservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-cacliente',
  templateUrl: './cacliente.page.html',
  styleUrls: ['./cacliente.page.scss'],
})
export class CaclientePage implements OnInit {
  public  cacliente = {
    "idtipocuenta": "",
    "nombre": "",
    "ap": "",
    "am": "",
    "email":"",
    "user": "",
    "password": "",
    "idIB": "",
    "idtrader": "",
    "numerocuenta": "",
    "documento":"",
    "tipodocumento":"",
    "fechaapertura":"",
    "telparticular":"",
    "teloficina":"",
    "telmovil":"",
    "ciudad":"",
    "pais":"",
    "direccion":"",
    "ocupacion":"",
    "observaciones": "",
    "estatus":""
  }
  public editar = false;
  public opc = false;
  public tipoactualizacion: any;
  public resposeData:any;
  public resposeDataON: any;
  public resposeDataTipoCuenta: any;
  public resposeDataTrader: any;
  public resposeDataIB: any;
  public filter:any;

  constructor(public authService: AuthserviceService, private snackBar: MatSnackBar) { }

  ngOnInit(){
    this.resposeDataON = [];
    this.authService.getData("consultatipocuenta").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultatipocuenta){
       this.resposeDataTipoCuenta = this.resposeData.consultatipocuenta;
       console.log("Tipo usuario");
       console.log(this.resposeDataTipoCuenta); 
      }
    });

    this.authService.getData("consultacacliente").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacacliente){
       this.resposeDataON = this.resposeData.consultacacliente;
       console.log(this.resposeDataON);
       this.filter = this.resposeData.consultacacliente;
      }else{
       this.snackBar.open("Error","Infomacion no disponible.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });

    this.authService.getData("consultaTrader").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultaTrader){
       this.resposeDataTrader = this.resposeData.consultaTrader;
      }
    });

    this.authService.getData("consultacaib").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.consultacaib){
       this.resposeDataIB = this.resposeData.consultacaib;
      }
    });

  }

  agregar(){
    this.editar = true;
    this.opc = false;
    this.cacliente = {
      "idtipocuenta": "",
      "nombre": "",
      "ap": "",
      "am": "",
      "email":"",
      "user": "",
      "password": "",
      "idIB": "",
      "idtrader": "",
      "numerocuenta": "",
      "documento":"",
      "tipodocumento":"",
      "fechaapertura":"",
      "telparticular":"",
      "teloficina":"",
      "telmovil":"",
      "ciudad":"",
      "pais":"",
      "direccion":"",
      "ocupacion":"",
      "observaciones": "",
      "estatus":""
    };  
    this.tipoactualizacion = "inserta";
    this.random();
  }

  random(){
    this.cacliente.numerocuenta = (Math.floor(100000 * Math.random())).toString();
    console.log(this.cacliente.numerocuenta);
    for(var i = 0; i < this.resposeDataON.length; i++){
        if(this.resposeDataON[i].numerocuenta == this.cacliente.numerocuenta){
            this.random();
        }
    }
  }

  editarR(data){
    this.editar = true;
    this.opc = false;
    this.tipoactualizacion = "edita";
    this.cacliente = data;
  }

  guardar(){
    if(this.tipoactualizacion == "inserta"){
    this.authService.postData(this.cacliente, "registrocacliente").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Guardado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
   if(this.tipoactualizacion == "edita"){
    this.authService.postData(this.cacliente, "updatecacliente").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
       this.snackBar.open("Exito","Registro editado exitosamente.",{
        duration: 3000,
        verticalPosition: "bottom"
       });
       this.editar= false;
       this.ngOnInit();
      }else{
       this.snackBar.open("Error","No se guardo el registro.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
       this.editar = false;
      }
    });
   }
  }

  eliminar(data){
    this.authService.postData(data, "deletecacliente").then((result) =>{
      this.resposeData = result;
      if(this.resposeData.exito){
        this.snackBar.open("Exito","Eliminacion exitosa.",{
          duration: 2000,
          verticalPosition: "bottom"
        });
        this.ngOnInit();
      }else{
       this.snackBar.open("Error","Registro no eliminado.",{
         duration: 3000,
         verticalPosition: "bottom"
       });
      }
    });
  }

  getItems(evt) {
    this.resposeDataON = this.filter;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }else{
     this.resposeDataON = this.resposeDataON.filter(item => {
      if (item.nombre && searchTerm || item.numerocuenta && searchTerm) {
        if ((item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) ||
            (item.numerocuenta.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)) {
          return true;
        }
        return false;
      }
     });
    }
  }

  info(data){
    console.log(data);
    this.editar = true;
    this.opc = true;
    this.cacliente = data;

  }
}